#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
pimac : pimac is a mail checker

Pimac is desiged to check all the email process, reception and
reading. Pimac sends an email to a address specified by option and
open an imap connection to server, fetch the last message and check if
is the wanted email.

'''
import sys
import imaplib
from socket import gethostbyname
from random import random
import smtplib
from time import sleep
from email.mime.text import MIMEText
from optparse import OptionParser
from email.parser import HeaderParser

def readimap(opts):
    """Connect to imap server, read last message quit"""
    server = opts.imap_server
    username = opts.username
    password = opts.password

    print "Read last email in imap"
    print " - server   : %s (%s)" % (server, gethostbyname(server))
    print " - username : %s" % (username)

    conn = imaplib.IMAP4(server)
    conn.login(username, password)
    # if it's called INBOX, then…
    typ, data = conn.select("INBOX")
    num_msgs = int(data[0])
    print 'There are %d messages in INBOX' % num_msgs

    data = conn.fetch(num_msgs, '(BODY[HEADER])')
    header_data = data[1][0][1]

    parser = HeaderParser()
    msg = parser.parsestr(header_data)
    print msg['Subject']

    conn.close()
    conn.logout()

def readopt():
    """Read and return commad line options"""
    parser = OptionParser() 
    parser.add_option("-f", action="store", type="string", dest="msgfrom", default = "foo@bar.com")
    parser.add_option("-t", action="store", type="string", dest="msgto", 
                      default = None, help="Message recipient the To: field")
    parser.add_option("-i", action="store", type="string", 
                      dest="imap_server", 
                      default = None,
                      help="imap server")
    parser.add_option("-r", action="store", type="string", dest="msgid", default = int(10e11 * random()))
    parser.add_option("-u", action="store", type="string", dest="username")
    parser.add_option("-p", action="store", type="string", dest="password", default=None)
    parser.add_option("-s", "--smtp", 
                      action="store", type="string", dest="smtp", 
                      help="SMTP server to use for sending email")
    parser.add_option("--smtp-login", 
                      action="store", type="string", dest="smtp_login", 
                      help="Login for authentication on SMTP serveur")
    parser.add_option("--smtp-password", 
                      action="store", type="string", dest="smtp_password", 
                      help="Password for authentication on SMTP serveur")
    parser.add_option("-n", "--no-send", 
                      action = "store_true", 
                      default = False, dest = "no_smtp", 
                      help="Do not send email before checking imap")
    (options, args) = parser.parse_args()

    if checkopts(options) != 0:
        print ""
        parser.print_help()
        sys.exit(1)

    return options

def sendmail(opts):
    """Send an email"""
    # Open a plain text file for reading.  For this example, assume that
    # the text file contains only ASCII characters.
    msg = MIMEText("Pimac id : %s" % opts.msgid)

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = '- Pimac check %s Pimac -' % opts.msgid
    msg['From'] = opts.msgfrom
    msg['To'] = opts.msgto

    print "Send email"
    print " - stmp : %s (%s)" % (opts.smtp, gethostbyname(opts.smtp))
    print " - to   : %s " % (msg['To'])
    print " - from : %s " % (msg['From'])
    print " - id   : %s " % (opts.msgid)

    if opts.smtp_login == None:
        sendmail_ano(opts, msg)
    else:
        sendmail_auth(opts, msg)

def sendmail_ano(opts, msg):
    """Send email without authentication"""
    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    smtp = smtplib.SMTP(opts.smtp)
    try:
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
    except smtplib.SMTPRecipientsRefused, error:
        print "Relay access denied"
        smtp.quit()
        sys.exit(2)
    smtp.quit()


def sendmail_auth(opts, msg):
    """Send email with authentication"""
    server = smtplib.SMTP(opts.smtp)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(opts.smtp_login,opts.smtp_password)

    try:
        server.sendmail(msg['From'], msg['To'], msg.as_string())
    except smtplib.SMTPRecipientsRefused, error:
        print "Relay access denied"
        server.quit()
        sys.exit(2)
    server.quit()

def waiter():
    """The waiter
    Really need to be rewrite
    """
    i = 4
    while i > 0:
        print "%s..." % i
        i = i - 1
        sleep(1)

def checkopts(opts):
    """Check mandatory options"""
    res = 0
    if opts.imap_server == None:
        print "It's better with an imap server, add -i imap_server_address"        
        res = 1
    if not opts.no_smtp and opts.msgto == None:
        print "Can we send an email without address ? add -t email@domina.com"
        res = 1
    if opts.password == None:
        print "Hey we need a password too, add -p YOU_PASSWORD"
        res = 1
    if opts.username == None:
        print "Add -u username"
        res = 1
    return res

def main():
    """Main programm"""
    options = readopt()
    if not options.no_smtp:
        sendmail(options)
        waiter()
    readimap(options)

if __name__ == "__main__":
    main()
